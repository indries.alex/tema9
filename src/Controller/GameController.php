<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GameController extends AbstractController{

     /**
      * @Route("/guesses/numbers")
      */
      public function guessNumber(){
          $number = random_int(0,10);
          $_SESSION["guess_number"] = $number;

          return $this->render('guess.html.twig');

      }

}