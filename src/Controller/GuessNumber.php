<?php
// src/Controller/GuessNumber.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GuessNumber extends AbstractController{

     /**
      * @Route("/guess/number")
      */
      public function numberGuess(){


          return $this->render('guess.html.twig');

      }
}