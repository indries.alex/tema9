<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Suma
{
     /**
      * @Route("/suma")
      */
    public function suma2Nr()
    {
        $number = random_int(0, 100);
        $number1 = random_int(0,100);

        $sumaNr= $number + $number1;

        return new Response(

            '<html><body>Suma 2 numere random('.$number.', '.$number1.'): '.$sumaNr.'</body></html>'

        );
    }
}