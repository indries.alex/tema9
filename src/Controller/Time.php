<?php
// src/Controller/LuckyController.php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class Time extends AbstractController
{
     /**
      * @Route("/time")
      */
    public function time()
    {
        
        date_default_timezone_set("Europe/Bucharest");

        
            $data = date("Y/m/d");
            $ora = date("h:i:sa");
            

            return $this->render('time.html.twig', [
                'data' => $data,
                'ora' => $ora
            ]);

    }

     /**
      * @Route("/ajax/time")
      */
    public function ajaxTime(){
        date_default_timezone_set("Europe/Bucharest");
        $ora = date("h:i:sa");
        return new Response($ora);
    }
    
}